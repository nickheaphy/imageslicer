# https://www.youtube.com/watch?v=HLaLVyIfb7g&ab_channel=TraceLoops

from PIL import Image, ImageDraw, ImageStat
import random

thepic = "hand.png"
number_of_splits = 20

# The copy and paste arrays determine the order of cuts/pastes
copy_order = [0, 1]
paste_order = [0, 0, 1, 1]

# dirty method to handle horizontal cuts 
rotate_first = False

def halftone(orig_im, sample = 10, scale = 1):
    '''Halftone the image'''
    # https://stackoverflow.com/questions/10572274/how-to-create-cmyk-halftone-images-from-a-color-image/10575940#10575940
    angle = 15
    new_im = orig_im.rotate(angle, expand=True)
    size = new_im.width*scale, new_im.height*scale
    half_tone = Image.new('L', size)
    draw = ImageDraw.Draw(half_tone)

    # Cycle through the image, drawing points
    for x in range(0, new_im.width, sample):
        for y in range(0, new_im.height, sample):
            # Sample Area
            box = new_im.crop((x, y, x + sample, y + sample))
            # Average Grayscale Level
            mean = ImageStat.Stat(box).mean[0]

            # The diameter of the circle to draw based on the mean (0-1)
            diameter = (mean / 255) ** 0.5

            # Size of the box we'll draw the circle in:
            box_size = sample * scale

            # Diameter of circle we'll draw:
            # If sample=10 and scale=1 then this is (0-10)
            draw_diameter = diameter * box_size

            # Position of top-left of box we'll draw the circle in:
            # x_pos, y_pos = (x * scale), (y * scale)
            box_x, box_y = (x * scale), (y * scale)

            # Positioned of top-left and bottom-right of circle:
            # A maximum-sized circle will have its edges at the edges
            # of the draw box.
            x1 = box_x + ((box_size - draw_diameter) / 2)
            y1 = box_y + ((box_size - draw_diameter) / 2)
            x2 = x1 + draw_diameter
            y2 = y1 + draw_diameter

            draw.ellipse([(x1, y1), (x2, y2)], fill=255)
        
    half_tone = half_tone.rotate(-angle, expand=True)
    width_half, height_half = half_tone.size

    # Top-left and bottom-right of the image to crop to:
    xx1 = (width_half - orig_im.width * scale) / 2
    yy1 = (height_half - orig_im.height * scale) / 2
    xx2 = xx1 + orig_im.width * scale
    yy2 = yy1 + orig_im.height * scale

    half_tone = half_tone.crop((xx1, yy1, xx2, yy2))
 
    return half_tone

def pattern_slice(orig_im, chunk_px, copy_order, paste_order, rotate_first=False):
    """Slices the orig_im based on the pattern.
    The copy_order pattern is a list of numbers like [0,1,2]
    The paste_order pattern says how the shuffle the slices up

    Args:
        orig_im (PIL image): The Original Image
        chunk_px (int): The width of the slice
        copy_order ([List]: [description]
        paste_order ([List]): [description]
        rotate_first (bool, optional): Should the image be rotated before processing. Defaults to False.

    Returns:
        PIL image: The processed image
    """

    if rotate_first:
        orig_im = orig_im.rotate(angle=90, expand=True)
    
    # now look at the length of the cut vrs paste to determine the final size
    img_scale = len(paste_order)/len(copy_order)

    # Create a new image to hold the slices
    new_im = Image.new(orig_im.mode,(int(orig_im.width*img_scale), orig_im.height))

    # Now copy the original image in slices into the new image
    start_orig_pos, start_new_pos = 0, 0

    while start_orig_pos < orig_im.width:
        for copy_seq in copy_order:
            # now copy the correct slice
            copy_from_x1 = start_orig_pos + copy_seq*chunk_px
            copy_from_x2 = copy_from_x1 + chunk_px
            im_crop = orig_im.crop((copy_from_x1,0,copy_from_x2, orig_im.height))

            for i, paste_seq in enumerate(paste_order):
                if copy_seq == paste_seq:
                    paste_to_x1 = start_new_pos + i * chunk_px
                    new_im.paste(im_crop,(paste_to_x1,0))

        # update the start positions
        start_new_pos += len(paste_order) * chunk_px
        start_orig_pos += len(copy_order) * chunk_px

    if rotate_first:
        new_im = new_im.rotate(angle=-90, expand=True)

    return new_im
    

def main():
    orig_im = Image.open(thepic).convert('LA')

    #orig_im = halftone(orig_im, 5, 5)
    # halftone_img.show()
    #determine the chunk size
    #use the shortest edge
    if orig_im.width < orig_im.height:
        chunk_px = int(orig_im.width / number_of_splits)
    else:
        chunk_px = int(orig_im.height / number_of_splits)
    
    orig_im = pattern_slice(orig_im, chunk_px, copy_order, paste_order, False)
    orig_im = pattern_slice(orig_im, chunk_px, copy_order, paste_order, True)

    orig_im.show()

if __name__ == "__main__":
    main()