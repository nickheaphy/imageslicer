# ImageSlicer

I found the amazing art of TraceLoops https://www.instagram.com/traceloops/ and thought that would be an interesting Sunday Covid19 lockdown challange to implement his physical image slicing art in code.

![Hand](sliced_hand.png)

After getting the `patternslice.py` working I thought it would be interesting to animate it and give it the cut edges from TraceLoops original, hence `animated_patternslice.py` (I did not include the resulting gif in the repositry as it is huge - was tool lazy to do any optimisation)

While I did try using halftoned versions of the image (as per TraceLoop) I prefered the non-halftoned output.