# https://www.youtube.com/watch?v=HLaLVyIfb7g&ab_channel=TraceLoops

from PIL import Image, ImageDraw, ImageStat
import random

thepic = "hand.png"
number_of_splits = 22

# The copy and paste arrays determine the order of cuts/pastes
copy_order = [0, 1]
paste_order = [0, 0, 1, 1]


def cut_edges(orig_im):
    """Simulates cut edges on left and right edges

    Args:
        orig_im (PILImage): The image to apply the edges to

    Returns:
        PILImage: The version with cut edges
    """    

    width_px = int(orig_im.width * 0.05)
    height_px = int(orig_im.height * 0.005)

    # build the edge
    left_edge = [(0,0)]
    start = 0
    while start < orig_im.height:
        # random amounts
        x_pos = random.randrange(-width_px*5,width_px)
        y_pos = random.randrange(start, start+height_px)
        left_edge.append((x_pos, y_pos))
        start += y_pos
    left_edge.append((-10, start))

    right_edge = [(orig_im.width,0)]
    start = 0
    while start < orig_im.height:
        # random amounts
        x_pos = random.randrange(-width_px,width_px*5)
        y_pos = random.randrange(start, start+height_px)
        right_edge.append((x_pos+orig_im.width, y_pos))
        start += y_pos
    right_edge.append((orig_im.width+width_px, start))
    
    draw = ImageDraw.Draw(orig_im)
    draw.polygon(left_edge,fill=(255,50))
    draw.polygon(right_edge,fill=(255,50))

    return orig_im


def pattern_slice_animation(orig_im, anim_frame_dim, chunk_px, copy_order, paste_order, rotate_first=False, randoffset=False):
    """Slices the orig_im based on the pattern.
    The copy_order pattern is a list of numbers like [0,1,2]
    The paste_order pattern says how the shuffle the slices up

    Args:
        orig_im (PIL image): The Original Image
        anim_frame_dim: A tuple of the width and height of the animation
        chunk_px (int): The width of the slice
        copy_order ([List]: [description]
        paste_order ([List]): [description]
        rotate_first (bool, optional): Should the image be rotated before processing. Defaults to False.

    Returns:
        PIL image: The processed image
    """

    anim_frames = []

    if rotate_first:
        orig_im = orig_im.rotate(angle=90, expand=True)
    
    # now look at the length of the cut vrs paste to determine the final size
    img_scale = len(paste_order)/len(copy_order)

    # Create a new image to hold the slices
    new_im = Image.new(orig_im.mode,(int(orig_im.width*img_scale), orig_im.height))

    # For the animation, copy the original into the new image
    new_im.paste(orig_im,(0,0))

    # To make the animations more interesting, going to reverse the drawing order and start at the right edge
    # I'm gonna cheat a little, and process in the forward order like the orignal code but then, storing the postion data into a list,
    # then just iterate the list in reverse to do the animations

    copy_from_xywh = []
    paste_to_xy = []

    start_orig_pos, start_new_pos = 0, 0

    while start_orig_pos < orig_im.width:
        for copy_seq in copy_order:
            # now copy the correct slice
            copy_from_x1 = start_orig_pos + copy_seq*chunk_px
            copy_from_x2 = copy_from_x1 + chunk_px
            
            #store this slice dimensions
            copy_from_xywh.append([copy_from_x1,0,copy_from_x2,orig_im.height])
            
            paste_pos = []
            for i, paste_seq in enumerate(paste_order):
                if copy_seq == paste_seq:
                    paste_to_x1 = start_new_pos + i * chunk_px
                    paste_pos.append([paste_to_x1,0])
            paste_to_xy.append(paste_pos)
                    
        # update the start positions
        start_new_pos += len(paste_order) * chunk_px
        start_orig_pos += len(copy_order) * chunk_px

    #now we should have a couple of lists to process
    for i, slice in reversed(list(enumerate(copy_from_xywh))):
        # Grab the slice
        im_crop = orig_im.crop((slice[0],slice[1],slice[2],slice[3]))

        for paste_slice in reversed(paste_to_xy[i]):
            
            # clear the old position
            new_im.paste(0,(slice[0],slice[1],slice[2],slice[3]))
            # paste the new image
            if randoffset:
                randoffsetamount = random.randrange(-4,4)
            else:
                randoffsetamount = 0
            new_im.paste(cut_edges(im_crop),(paste_slice[0],paste_slice[1]+randoffsetamount))
            # create a new frame
            anim_frame = Image.new(orig_im.mode,(anim_frame_dim[0], anim_frame_dim[1]))
            # paste the frame
            if rotate_first:
                temp = new_im.rotate(angle=-90, expand=True)
            else:
                temp = new_im
            anim_frame.paste(temp,(0,0))
            anim_frames.append(anim_frame)

    if rotate_first:
        new_im = new_im.rotate(angle=-90, expand=True)

    return new_im, anim_frames
    

def main():
    orig_im = Image.open(thepic).convert('LA')

    #determine the chunk size
    #use the shortest edge
    if orig_im.width < orig_im.height:
        chunk_px = int(orig_im.width / number_of_splits)
    else:
        chunk_px = int(orig_im.height / number_of_splits)

    # animation size
    anim_width = int(orig_im.width * len(paste_order)/len(copy_order))
    anim_height = int(orig_im.height * len(paste_order)/len(copy_order))
    
    orig_im, anim_frames = pattern_slice_animation(orig_im, (anim_width, anim_height), chunk_px, copy_order, paste_order, False)
    orig_im, anim_frames2 = pattern_slice_animation(orig_im, (anim_width, anim_height), chunk_px, copy_order, paste_order, True, True)

    anim_frames += anim_frames2

    # zoom the final frame
    for i in range(1,8):
        zoom_frame = anim_frames[-1].resize((int(anim_frames[-1].width*(1+0.1*i)),int(anim_frames[-1].height*(1+0.1*i))))
        zoom_frame_cropped = Image.new(anim_frames[-1].mode,(anim_frames[-1].width,anim_frames[-1].height))
        zoom_frame_cropped.paste(zoom_frame, (int(-((zoom_frame.width-zoom_frame_cropped.width)/2)), int(-((zoom_frame.height-zoom_frame_cropped.height)/2))))
        anim_frames.append(zoom_frame_cropped)

    bounce = anim_frames + list(reversed(anim_frames))

    orig_im.save('sliced_hand.png')
    bounce[0].save('sliced_anim.gif', format='GIF', append_images=bounce[1:], save_all=True, duration=1, loop=0)

if __name__ == "__main__":
    main()